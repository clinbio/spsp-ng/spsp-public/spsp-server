# SPSP Server

## Description
The SPSP Server is part of the backend. It manages all the data, data flows and logic behind SPSP.

Sensitive data, like original sample identifiers, may be submitted to SPSP. SPSP is therefore hosted on a secure IT infrastructure (BioMedIT SENSA). As a consequence, the source code for the SPSP Server may not be public, as it would provide information on the API endpoints, database model and general architecture that could eventually make the platform vulnerable.

The source code for the SPSP Server may however be shared upon request by trusted institutions. Please contact <spsp-support@sib.swiss>.

## Authors
Main developer: Daniel Walther

## License
For more information or any inquiries, please reach out to legal@sib.swiss.